package com.btsinfo.assistant;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class LoginActivity extends Activity {
    EditText et_login;
    EditText et_password;
    Button BT_Connexion;
    Button BT_DB;
    Button BT_Inscription;
    DatabaseManager DB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_login);

        // Cacher la barre de navigation en bas du téléphone
        this.getWindow().getDecorView().setSystemUiVisibility(
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        DB = new DatabaseManager(this);

        final EditText et_login = (EditText) findViewById(R.id.et_login);
        final EditText et_password = (EditText) findViewById(R.id.et_password);

        // Bouton de connexion
        BT_Connexion=(Button) findViewById(R.id.BT_Connexion) ;
        BT_Connexion.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
            String login = et_login.getText().toString();
            String password = et_password.getText().toString();
            String type = "login";

            BackgroundWorker bkgw = new BackgroundWorker(LoginActivity.this);
            bkgw.execute(type,login,password);

            } }) ;

        // Bouton accès SQLite Manager
        BT_DB=(Button) findViewById(R.id.BT_DB);
        BT_DB.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            Intent dbmanager = new Intent(LoginActivity.this,AndroidDatabaseManager.class);
            startActivity(dbmanager);
            }
        });

        // Bouton inscription
        BT_Inscription=(Button) findViewById(R.id.BT_Inscription);
        BT_Inscription.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            Intent intent = new Intent(LoginActivity.this, InscriptionActivity.class);
            startActivity(intent);
            }
        });

        // Test si l'utilisateur a un accès internet
        if(isNetworkAvailable()==true){
            Toast.makeText(getApplicationContext(),"Mode connecté",Toast.LENGTH_LONG).show();
        }
        else if(isNetworkAvailable()==false && DB.GetUsrConnected()=="neverConnected") {
            Toast.makeText(getApplicationContext(),"Mode hors-ligne",Toast.LENGTH_LONG).show();
        }
        else{
            Toast.makeText(getApplicationContext(),"Mode hors-ligne",Toast.LENGTH_LONG).show();
            Intent intent = new Intent(LoginActivity.this, AssistantActivity.class);
            startActivity(intent);
        }
    }

    // Détection si l'utilisateur a une connexion internet
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
