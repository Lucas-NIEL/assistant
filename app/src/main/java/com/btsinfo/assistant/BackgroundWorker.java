package com.btsinfo.assistant;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class BackgroundWorker extends AsyncTask<String, Void, String> {
    Context context;
    String id;
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    DatabaseManager DB;

    AlertDialog alertDialog;

BackgroundWorker (Context ctx){
    context = ctx;
}

    @Override
    protected String doInBackground(String... params) {
        String type = params[0];

        String login_url = "http://lniel.bts-sio.com/android/login.php";
        String inscription_url = "http://lniel.bts-sio.com/android/inscription.php";
        //String categories_url = "http://lniel.bts-sio.com/android/categories.php";

        //String login_url = "http://10.0.2.2/android/login.php";
        //String inscription_url = "http://10.0.2.2/android/inscription.php";
        //String categories_url = "http://10.0.2.2/android/categories.php";

        // L'utilisateur vient pour se connecter
        if (type.equals("login"))
        {
            try {
                String login = params[1];
                String mdp = params[2];
                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                String post_donnees = URLEncoder.encode("login","UTF-8")+"="+URLEncoder.encode(login,"UTF-8")+"&" +
                        URLEncoder.encode("mdp","UTF-8") + "=" + URLEncoder.encode(mdp,"UTF-8");
                bufferedWriter.write(post_donnees);
                bufferedWriter.flush();
                bufferedWriter.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"));
                String result = "";
                String ligne = "";
                while((ligne=bufferedReader.readLine()) != null)
                {
                    result += ligne;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // L'utilisateur vient pour s'inscrire
        if (type.equals("inscription")){
            try {
                String login = params[1];
                String mdp = params[2];
                String mail = params[3];
                URL url = new URL(inscription_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                String post_donnees = URLEncoder.encode("login","UTF-8")+"="+URLEncoder.encode(login,"UTF-8")
                        +"&" + URLEncoder.encode("mdp","UTF-8") + "=" + URLEncoder.encode(mdp,"UTF-8")
                        +"&" + URLEncoder.encode("mail","UTF-8") + "=" + URLEncoder.encode(mail,"UTF-8");
                bufferedWriter.write(post_donnees);
                bufferedWriter.flush();
                bufferedWriter.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"));
                String result = "";
                String ligne = "";
                while((ligne=bufferedReader.readLine()) != null)
                {
                    result += ligne;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle("État de la connexion");
    }

    @Override
    protected void onPostExecute(String result) {
        // Test si connexion réussie
        if(result.contains("connexion"))
        {
            String[] split = result.split("connexion");
            String Id = split[0];
            String login = split[1];

            Intent intent = new Intent(context, AssistantActivity.class);
            intent.putExtra("usr_id",Id);
            setId(Id.toString());

            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            String today = dateFormat.format(date);
            DB = new DatabaseManager(context);
            DB.SetUsrConnected(Id, today);

            context.startActivity(intent);
            Toast.makeText(this.context,"Connexion réussie : "+login,Toast.LENGTH_SHORT).show();
        }
        else{
            Toast.makeText(this.context,"Échec de la connexion",Toast.LENGTH_SHORT).show();
        }

        // Test si insciption réussie
        if(result.contains("inscription"))
        {
            String[] split = result.split("inscription");

            Intent intent = new Intent(context, LoginActivity.class);
            context.startActivity(intent);

            Toast.makeText(this.context,"Inscription réussie !", Toast.LENGTH_SHORT).show();
        }
        // Test si échec
        if(result == null){
            alertDialog.setMessage(result);
            alertDialog.show();
        }
    }


    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }


}


