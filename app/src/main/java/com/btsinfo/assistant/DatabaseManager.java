package com.btsinfo.assistant;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.MatrixCursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class DatabaseManager extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "assistant_db";
    public static final int DATABASE_VERSION = 1;

    public DatabaseManager(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        SQLiteDatabase DB = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL("CREATE TABLE task (task_id INTEGER PRIMARY KEY AUTOINCREMENT, task_title VARCHAR, task_desc VARCHAR, task_usr_id INTEGER, task_cat_id INTEGER, task_date VARCHAR)");
        db.execSQL("CREATE TABLE category (cat_id INTEGER PRIMARY KEY AUTOINCREMENT, cat_title VARCHAR UNIQUE, cat_color INTEGER, cat_usr_id INTEGER)");
        db.execSQL("CREATE TABLE note (note_usr_id INTEGER PRIMARY KEY, note_text TEXT)");
        db.execSQL("CREATE TABLE usr_connected (usr_id PRIMARY KEY, date VARCHAR)");
        //db.execSQL("INSERT INTO category VALUES(1, 'Aucune', '-1', 0)");
        Log.i("DATABASE","onCreated invoked");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_NAME);
        onCreate(db);
    }

    // ************************************************************
    // ******************** Mes insertions ************************
    // ************************************************************

    /****** TÂCHES ******/
    public boolean newTask(String task_title, String task_desc, String task_usr_id, long task_cat_id, String task_date)
    {
        boolean success=false;
        try{
            String strSqlNewTask = "INSERT INTO task (task_title, task_desc, task_usr_id, task_cat_id, task_date) VALUES ('"+task_title+"','"+task_desc+"','"+task_usr_id+"','"+task_cat_id+"','"+task_date+"')";
            this.getWritableDatabase().execSQL(strSqlNewTask);
            success=true;
        }catch (SQLException e)
        {
            success=false;
        }
        return success;
    }

    public boolean deleteTask(long ptask_id)
    {
        boolean success=false;

        try{
            //String strSqlDeleteTask = "DELETE FROM task WHERE task_id="+ptask_id+")";
            String strTask_ID = Long.toString(ptask_id);
            this.getWritableDatabase().delete("task", "task_id=?", new String[]{strTask_ID});
            success=true;
        }catch (SQLException e)
        {
            success=false;
        }
        return success;
    }

    // Insertion nouvelle catégorie
    public void newCategorie(String cat_title, Integer cat_color, String usr_id)
    {
        String strSqlNewTask = "INSERT INTO category (cat_title, cat_color, cat_usr_id) VALUES ('"+cat_title+"','"+cat_color+"','"+usr_id+"')";
        this.getWritableDatabase().execSQL(strSqlNewTask);
    }

    // Insertion user connecté
    public void SetUsrConnected(String usr_id, String date)
    {
        this.getWritableDatabase().execSQL("DELETE FROM usr_connected");
        String strSqlUsr_Connected= "INSERT INTO usr_connected (usr_id, date) VALUES ('"+usr_id+"','"+date+"')";
        this.getWritableDatabase().execSQL(strSqlUsr_Connected);
    }

    // Insertion ou update d'une note
    // CREATE TABLE note (note_id, note_text, note_usr_id)
    public boolean newNote(String usr_id, String note)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        SQLiteStatement stmt = db.compileStatement("INSERT OR REPLACE INTO note VALUES ('"+usr_id+"', ?)");
        stmt.bindString(1, note);

        boolean success = false;

        //String strSqlNewNote = "INSERT INTO note VALUES (null, '"+note+"', '"+usr_id+"') ON DUPLICATE KEY UPDATE note_text='"+note+"'";

        try{
            stmt.execute();
            //db.execSQL("INSERT OR REPLACE INTO note VALUES ('"+usr_id+"', '"+ note +"')");
            success = true;
        }catch (SQLException e)
        {
            success = false;
        }
        return success;
    }

    // Modification d'une tâche
    public boolean updateTask(long task_id, String task_title, String task_desc, int task_cat_id, String task_date)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        boolean success=false;

        try {
            db.execSQL("UPDATE task SET task_title='"+task_title+"', task_desc='"+task_desc+"', task_cat_id='"+task_cat_id+"', task_date='"+task_date+"' WHERE task_id='"+task_id+"'");
            success=true;
        }catch (SQLException e)
        {
            success=false;
        }
        return success;
    }

    // ******************************************************
    // ******************** Mes lectures ********************
    // ******************************************************

    // Récupération des tâches de l'utilisateur
    public List<Tache> getTasks(String usr_id)
    {
        List<Tache> taches = new ArrayList<>();
        String strSqlGetTasks="SELECT * FROM task WHERE task_usr_id="+usr_id;
        Cursor curseurTasks = this.getReadableDatabase().rawQuery(strSqlGetTasks,null);
        curseurTasks.moveToFirst();

        while(!curseurTasks.isAfterLast())
        {
            Tache tache = new Tache(curseurTasks.getInt(0),curseurTasks.getString(1),curseurTasks.getString(2),curseurTasks.getInt(3),curseurTasks.getInt(4));
            taches.add(tache);
            curseurTasks.moveToNext();
        }
        curseurTasks.close() ;
        return taches;
    }

    // Récupération des catégories de l'utilisateur
    public List<String> getCategories (String usr_id)
    {
        List<String> categories = new ArrayList<>();
        String strSqlGetCategories="SELECT * FROM category WHERE cat_usr_id="+usr_id;
        Cursor curseurCategories = this.getReadableDatabase().rawQuery(strSqlGetCategories,null);
        curseurCategories.moveToFirst();

        while(!curseurCategories.isAfterLast())
        {
            String categorie = curseurCategories.getString(1);
            categories.add(categorie);
            curseurCategories.moveToNext();
        }
        curseurCategories.close();
        return categories;
    }

    // Récupération du nom d'une catégorie par son ID
    public String getNameCategoryById(long cat_id)
    {
        String cat_name=null;
        String strGetNameCategoryById="SELECT * FROM category WHERE cat_id="+cat_id;
        Cursor curseurTaskDetails = this.getReadableDatabase().rawQuery(strGetNameCategoryById,null);
        curseurTaskDetails.moveToFirst();

        if(curseurTaskDetails != null && curseurTaskDetails.moveToFirst())
        {
            cat_name = curseurTaskDetails.getString(curseurTaskDetails.getColumnIndex("cat_title"));
            curseurTaskDetails.close();
        }
        return cat_name;
    }

    // Récupération des Notes de l'utilisateur
    public String getNotes(String usr_id)
    {
        String note=null;
        String strGetNotes="SELECT * FROM note WHERE note_usr_id="+usr_id;
        Cursor curseurNote = this.getReadableDatabase().rawQuery(strGetNotes,null);
        curseurNote.moveToFirst();
        if(curseurNote != null && curseurNote.moveToFirst())
        {
            note = curseurNote.getString(curseurNote.getColumnIndex("note_text"));
            curseurNote.close();
        }
        return note;
    }

    // Récupération de la couleur d'une catégorie
    public String getColorByCatId(long cat_id)
    {
        String color=null;
        String strGetColor="SELECT cat_color FROM category WHERE cat_id="+cat_id;
        Cursor curseurColor = this.getReadableDatabase().rawQuery(strGetColor,null);
        curseurColor.moveToFirst();
        if(curseurColor != null && curseurColor.moveToFirst())
        {
            color = curseurColor.getString(curseurColor.getColumnIndex("cat_color"));
            curseurColor.close();
        }
        return color;
    }

    // Récupération de l'ID d'une catégorie par son nom
    public int getIdCategoryByName(String cat_title)
    {
        int cat_id=0;
        String strgetIdCategoryByName="SELECT * FROM category WHERE cat_title='"+cat_title+"'";
        Cursor curseurTaskDetails = this.getReadableDatabase().rawQuery(strgetIdCategoryByName,null);
        curseurTaskDetails.moveToFirst();

        if(curseurTaskDetails != null && curseurTaskDetails.moveToFirst())
        {
            cat_id = curseurTaskDetails.getInt(curseurTaskDetails.getColumnIndex("cat_id"));
            curseurTaskDetails.close();
        }
        return cat_id;
    }

    // Récupération des informations d'une tâche donnée en paramètre
    public Tache getTaskDetails(String task_id)
    {
        String strSqlGetTaskDetails="SELECT * FROM task WHERE task_id='"+task_id+"'";
        Cursor curseurTaskDetails = this.getReadableDatabase().rawQuery(strSqlGetTaskDetails,null);
        curseurTaskDetails.moveToFirst();

        //Tache tacheDetails = new Tache(1,"tite","desc",1,1);
        Tache laTache = new Tache();

        //Tache tacheDetails = new Tache (curseurTaskDetails.getInt(0),curseurTaskDetails.getString(1),curseurTaskDetails.getString(2),curseurTaskDetails.getInt(3),curseurTaskDetails.getInt(4));
        if(curseurTaskDetails != null && curseurTaskDetails.moveToFirst())
        {
            laTache.setTask_id(curseurTaskDetails.getInt(curseurTaskDetails.getColumnIndex("task_id")));
            laTache.setTask_title(curseurTaskDetails.getString(curseurTaskDetails.getColumnIndex("task_title")));
            laTache.setTask_desc(curseurTaskDetails.getString(curseurTaskDetails.getColumnIndex("task_desc")));
            laTache.setTask_usr_id(curseurTaskDetails.getInt(curseurTaskDetails.getColumnIndex("task_usr_id")));
            laTache.setTask_cat_id(curseurTaskDetails.getInt(curseurTaskDetails.getColumnIndex("task_cat_id")));
            laTache.setTask_date(curseurTaskDetails.getString(curseurTaskDetails.getColumnIndex("task_date")));
            curseurTaskDetails.close();
        }
        return laTache;
    }

    // Récupération usr_connecté
    public String GetUsrConnected()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor result = db.rawQuery("SELECT * FROM usr_connected",null);
        result.moveToFirst();
        String usr="0";

        if(result.getCount()==0)
        {
            usr ="neverConnected";
        }
        else{
            usr = result.getString(0);
        }

        return usr;
    }








    // *************** Android DatabaseManager ***************
    public ArrayList<Cursor> getData(String Query){
        //get writable database
        SQLiteDatabase sqlDB = this.getWritableDatabase();
        String[] columns = new String[] { "message" };
        //an array list of cursor to save two cursors one has results from the query
        //other cursor stores error message if any errors are triggered
        ArrayList<Cursor> alc = new ArrayList<Cursor>(2);
        MatrixCursor Cursor2= new MatrixCursor(columns);
        alc.add(null);
        alc.add(null);

        try{
            String maxQuery = Query ;
            //execute the query results will be save in Cursor c
            Cursor c = sqlDB.rawQuery(maxQuery, null);

            //add value to cursor2
            Cursor2.addRow(new Object[] { "Success" });

            alc.set(1,Cursor2);
            if (null != c && c.getCount() > 0) {
                alc.set(0,c);
                c.moveToFirst();
                return alc ;
            }
            return alc;
        } catch(SQLException sqlEx){
            Log.d("printing exception", sqlEx.getMessage());
            //if any exceptions are triggered save the error message to cursor an return the arraylist
            Cursor2.addRow(new Object[] { ""+sqlEx.getMessage() });
            alc.set(1,Cursor2);
            return alc;
        } catch(Exception ex){
            Log.d("printing exception", ex.getMessage());
            // if any exceptions are triggered save the error message to cursor an return the arraylist
            Cursor2.addRow(new Object[] { ""+ex.getMessage() });
            alc.set(1,Cursor2);
            return alc;
        }
    }


}
