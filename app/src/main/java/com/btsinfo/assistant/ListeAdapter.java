package com.btsinfo.assistant;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class ListeAdapter extends ArrayAdapter<Tache> {
    Context context;
    DatabaseManager DB;

    // Nous créons le constructeur de l'adaptateur à qui on indique la liste qui détient les données
    //qu'il devra gérer
    public ListeAdapter(Context context, List<Tache> listeTache) {
        super(context, -1, listeTache);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        // Déclaration d'une variable qui va contenir les données d'une tache
        Tache uneTache;
        view = null;
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.liste_ligne, parent, false);
        } else {
            view = convertView;
        }

        DB = new DatabaseManager(getContext());
        // Un élément de la liste
        uneTache = getItem(position);
        // Faire le lien entre la variable Java et le composant « TextView » de l'interface
        TextView task_title = (TextView) view.findViewById(R.id.task_title);
        TextView task_desc = (TextView) view.findViewById(R.id.task_desc);
        ImageView task_cat = (ImageView) view.findViewById(R.id.task_cat);
        Integer color = Integer.parseInt(DB.getColorByCatId(uneTache.task_cat_id));

        // Mettre le titre et la desc de la catégorie récupérée dans le « TextView »
        task_title.setText(uneTache.task_title);
        task_desc.setText(uneTache.task_desc);
        task_cat.setColorFilter(color);

        return view;
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).task_id;
    }
}
