package com.btsinfo.assistant;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import yuku.ambilwarna.AmbilWarnaDialog;

public class NewCategory extends Activity {
    int mDefaultColor;
    Button BT_Color;
    Button BT_Valider;
    TextView disp_color;
    EditText et_cat_title;
    DatabaseManager DB;
    String usr_id;
    ImageView display_selected_color;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_new_category);

        // Cacher la barre de navigation en bas du téléphone
        this.getWindow().getDecorView().setSystemUiVisibility(
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);


        DB = new DatabaseManager(this);
        usr_id = DB.GetUsrConnected();
        //Toast.makeText(getApplicationContext(),"ID : "+usr_id,Toast.LENGTH_SHORT).show();

        // Couleur par défaut récupérer si vient pour modifier catégorie
        mDefaultColor = 15;
        disp_color = (TextView) findViewById(R.id.color);
        display_selected_color = (ImageView) findViewById(R.id.display_selected_color);

        BT_Color = (Button) findViewById(R.id.BT_Color);
        BT_Color.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openColorPicker();
            }
        });

        // Bouton ajouter nouvelle catégorie
        BT_Valider = (Button) findViewById(R.id.BT_AddNewCat);
        BT_Valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int color = disp_color.getCurrentTextColor();
                et_cat_title = (EditText) findViewById(R.id.et_cat_title);
                String cat_title = et_cat_title.getText().toString();

                if(cat_title.length()>0)
                {
                    DB.newCategorie(cat_title, color, usr_id);
                    Toast.makeText(getApplicationContext(),"Ajout de la catégorie "+cat_title+" réussie !",Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(NewCategory.this, AssistantActivity.class);
                    startActivity(intent);
                }
            }
        });
    }

    public void openColorPicker()
    {
        AmbilWarnaDialog colorPicker = new AmbilWarnaDialog(this, mDefaultColor, new AmbilWarnaDialog.OnAmbilWarnaListener() {
            @Override
            public void onCancel(AmbilWarnaDialog dialog) {

            }

            @Override
            public void onOk(AmbilWarnaDialog dialog, int color) {
                mDefaultColor = color;
                disp_color.setTextColor(mDefaultColor);
                display_selected_color.setColorFilter(mDefaultColor);
                //Toast.makeText(getApplicationContext(),"Couleur : "+mDefaultColor,Toast.LENGTH_SHORT).show();
            }
        });
        colorPicker.show();
    }




}
