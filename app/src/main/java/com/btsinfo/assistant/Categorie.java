package com.btsinfo.assistant;

public class Categorie {
    long cat_id;
    String cat_title;
    String cat_color;
    long cat_usr_id;

    public Categorie(int cat_id, String cat_title, String cat_color, int cat_usr_id)
    {
        this.setCat_id(cat_id);
        this.setCat_title(cat_title);
        this.setCat_color(cat_color);
        this.setCat_usr_id(cat_usr_id);
    }

    @Override
    public String toString(){
        return cat_id + " -> " + cat_title + " : " + cat_color;
    }

    public long getCat_id() { return cat_id; }
    public void setCat_id(long cat_id) { this.cat_id = cat_id; }

    public String getCat_title() { return cat_title; }
    public void setCat_title(String cat_title) { this.cat_title = cat_title; }

    public String getCat_color() { return cat_color; }
    public void setCat_color(String cat_color) { this.cat_color = cat_color; }

    public long getCat_usr_id() { return cat_usr_id; }
    public void setCat_usr_id(long cat_usr_id) { this.cat_usr_id = cat_usr_id; }



}
