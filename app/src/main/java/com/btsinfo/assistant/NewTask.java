package com.btsinfo.assistant;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class NewTask extends Activity {
    final Calendar calendrier = Calendar.getInstance();
    EditText et_calendrier;
    Button BT_Ajouter;
    Spinner spinner;
    List<String> categories;
    String usr_id;

    DatabaseManager DB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_new_task);
        // Cacher la barre de navigation en bas du téléphone
        this.getWindow().getDecorView().setSystemUiVisibility(
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        DB = new DatabaseManager(this);

        // Récupération de l'ID de l'utilisateur
        Intent intent = new Intent(this, AssistantActivity.class);
        usr_id = DB.GetUsrConnected();

        //Toast.makeText(this,"DEBUG : "+usr_id,Toast.LENGTH_SHORT).show();

        // Création d'une liste des catégories
        List<String> categories = DB.getCategories(usr_id) ;

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                // TODO Auto-generated method stub
                calendrier.set(Calendar.YEAR, year);
                calendrier.set(Calendar.MONTH, monthOfYear);
                calendrier.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        EditText et_calendrier = (EditText) findViewById(R.id.et_date);
        et_calendrier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(NewTask.this, date, calendrier
                    .get(Calendar.YEAR), calendrier.get(Calendar.MONTH),
                    calendrier.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        // Récupération du Spinner déclaré dans le fichier main.xml de res/layout
        spinner = (Spinner) findViewById(R.id.spinner);

		// Liste à donner au spinner
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, categories);

        /* On definit une présentation du spinner quand il est déroulé (android.R.layout.simple_spinner_dropdown_item) */
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Enfin on passe l'adapter au Spinner
        spinner.setAdapter(adapter);

        // Bouton ajout nouvelle tâche
        BT_Ajouter=(Button) findViewById(R.id.BT_Ajouter) ;
        BT_Ajouter.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){

                final EditText et_title = (EditText) findViewById(R.id.et_title);
                final EditText et_desc = (EditText) findViewById(R.id.et_desc);
                final EditText et_date = (EditText) findViewById(R.id.et_date);
                final Spinner spinner = (Spinner) findViewById(R.id.spinner);

                // Récupération des champs
                String title = et_title.getText().toString();
                String desc = et_desc.getText().toString();
                String date = et_date.getText().toString();
                long categorie = spinner.getSelectedItemId()+1;

                if(DB.newTask(title,desc,usr_id,categorie,date))
                {
                    Toast.makeText(getApplicationContext(),"Votre tâche a bien été ajoutée",Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(NewTask.this, AssistantActivity.class);
                    startActivity(intent);
                }
            } });




    }

    private void updateLabel() {
        String myFormat = "MM/dd/yyyy";
        //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.FRENCH);
        EditText et_calendrier = (EditText) findViewById(R.id.et_date);
        et_calendrier.setText(sdf.format(calendrier.getTime()));
    }
}
