package com.btsinfo.assistant;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class DetailsTask extends AppCompatActivity {
    EditText et_title;
    EditText et_desc;
    Spinner spinner_cat;

    final Calendar calendrier = Calendar.getInstance();

    Button BT_Modifer;
    Button BT_Supprimer;
    Button BT_Retour;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_task);

        Intent intent = getIntent();
        String task_id = intent.getStringExtra("task_id");

        final DatabaseManager db  = new DatabaseManager(this);
        final Tache uneTache = db.getTaskDetails(task_id);
        final EditText et_calendrier = (EditText) findViewById(R.id.et_calendrier);
        final EditText et_title = (EditText) findViewById(R.id.et_title);
        final EditText et_desc = (EditText) findViewById(R.id.et_desc);
        final Spinner spinner_cat = (Spinner) findViewById(R.id.spinner_cat);

        // Toast.makeText(getApplicationContext(), task_id,Toast.LENGTH_LONG).show();

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            // TODO Auto-generated method stub
            calendrier.set(Calendar.YEAR, year);
            calendrier.set(Calendar.MONTH, monthOfYear);
            calendrier.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
            }

        };

        et_calendrier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            // TODO Auto-generated method stub
            new DatePickerDialog(DetailsTask.this, date, calendrier.get(Calendar.YEAR), calendrier.get(Calendar.MONTH), calendrier.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        et_title.setText(uneTache.getTask_title());
        et_desc.setText(uneTache.getTask_desc());
        et_calendrier.setText(uneTache.getTask_date());

        // Création d'une liste des catégories
        List<String> categories = db.getCategories(db.GetUsrConnected()) ;
        // Liste à donner au spinner
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, categories);

        /* On definit une présentation du spinner quand il est déroulé (android.R.layout.simple_spinner_dropdown_item) */
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Enfin on passe l'adapter au Spinner
        spinner_cat.setAdapter(adapter);
        spinner_cat.setSelection(adapter.getPosition(db.getNameCategoryById(uneTache.task_cat_id)));

        //Toast.makeText(this,"cat_id : "+uneTache.getTask_cat_id(),Toast.LENGTH_SHORT).show();
        //String.valueOf(uneTache.getTask_cat_id());

        // Bouton BT_Modifer
        BT_Modifer=(Button) findViewById(R.id.BT_Modifer);
        BT_Modifer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            int category = db.getIdCategoryByName(spinner_cat.getSelectedItem().toString());
            if(db.updateTask(uneTache.task_id, et_title.getText().toString(),et_desc.getText().toString(), category, et_calendrier.getText().toString()))
            {
                Toast.makeText(getApplicationContext(),"Votre tâche a bien été modifiée",Toast.LENGTH_LONG).show();
            }
            }
        });

        // Bouton BT_Retour
        BT_Retour=(Button) findViewById(R.id.BT_Retour);
        BT_Retour.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            Intent intent = new Intent(DetailsTask.this, AssistantActivity.class);
            startActivity(intent);
            }
        });

        // Bouton BT_Supprimer
        BT_Supprimer=(Button) findViewById(R.id.BT_Supprimer);
        BT_Supprimer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            AlertDialog.Builder ConfirmationSuppr = new AlertDialog.Builder(DetailsTask.this);
            ConfirmationSuppr.setTitle("Supprimer la tâche ?");
            ConfirmationSuppr.setMessage("Êtes-vous sûr de vouloir supprimer cette tâche ?");
            ConfirmationSuppr.setCancelable(false);
            ConfirmationSuppr.setPositiveButton("Supprimer", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                db.deleteTask(uneTache.task_id);
                Toast.makeText(DetailsTask.this, "Suppression de la tâche" , Toast.LENGTH_LONG).show();
                Intent intent = new Intent(DetailsTask.this, AssistantActivity.class);
                startActivity(intent);
                }
            });

            ConfirmationSuppr.setNegativeButton("Non", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //Toast.makeText(DetailsTask.this, "Fermeture du dialogue", Toast.LENGTH_SHORT).show();
                }
            });
            ConfirmationSuppr.show();
            }
        });
    }

    private void updateLabel() {
        String myFormat = "MM/dd/yyyy";
        //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.FRENCH);
        EditText et_calendrier = (EditText) findViewById(R.id.et_calendrier);
        et_calendrier.setText(sdf.format(calendrier.getTime()));
    }






}
