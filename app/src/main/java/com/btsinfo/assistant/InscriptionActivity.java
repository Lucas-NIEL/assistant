package com.btsinfo.assistant;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InscriptionActivity extends AppCompatActivity {
    EditText et_login;
    EditText et_mdp;
    EditText et_mdp2;
    EditText et_mail;
    Button BT_Inscrire;
    TextView tv_errorMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_inscription);

        // Cacher la barre de navigation en bas du téléphone
        this.getWindow().getDecorView().setSystemUiVisibility(
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        Inscription();
    }

    // Inscription
    public void Inscription () {
        Button BT_Inscrire = (Button)findViewById(R.id.BT_Valider);
        BT_Inscrire.setOnClickListener(
            new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Récupération des éléments
                    EditText et_login = (EditText)findViewById(R.id.et_login);
                    EditText et_mdp = (EditText)findViewById(R.id.et_mdp);
                    EditText et_mdp2 = (EditText)findViewById(R.id.et_mdp2);
                    EditText et_mail = (EditText)findViewById(R.id.et_mail);
                    TextView tv_errorMessage = (TextView) findViewById(R.id.tv_errorMessage);

                    // Test longueur mot de passe
                    if(et_mdp.getText().toString().length()<=6)
                    {
                        tv_errorMessage.setText("La taille du mot de passe doit être au minimum de 6 caractères !");
                    }
                    // Test confirmation du mot de passe
                    else if(!et_mdp.getText().toString().equals(et_mdp2.getText().toString()))
                    {
                        tv_errorMessage.setText("Erreur dans la confirmation du mot de passe !");
                    }
                    // Test longueur login
                    else if(et_login.getText().toString().length()<=3 || et_login.getText().toString().length()>=20)
                    {
                        tv_errorMessage.setText("La taille du login doit être entre 3 et 20 caractère !");
                    }
                    // Test validation du mail
                    else if(!validate(et_mail.getText().toString()))
                    {
                        tv_errorMessage.setText("Le mail n'est pas conforme !");
                    }
                    else
                    {
                        String type = "inscription";
                        String login = et_login.getText().toString();
                        String mdp = et_mdp.getText().toString();
                        String mail = et_mail.getText().toString();

                        BackgroundWorker bkgw = new BackgroundWorker(InscriptionActivity.this);
                        bkgw.execute(type,login,mdp,mail);
                    }
                }
            }
        );
    }

    // Fonctions de test du mail
    public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    public static boolean validate(String emailStr)
    {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(emailStr);
        return matcher.find();
    }




}