package com.btsinfo.assistant;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class Tab1ToDoListe extends Fragment {
    List<Tache> listeTache;
    ListView lvListe;
    DatabaseManager DB;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {

        View view = inflater.inflate(R.layout.tab1liste,container,false);

        lvListe = (ListView) view.findViewById(R.id.lvListe) ;
        lvListe.setStackFromBottom(false);

        lvListe.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startViewActivity(position);
            }
        });

        listeTache = new ArrayList<Tache>();

        DatabaseManager db  = new DatabaseManager(getContext());
        listeTache = db.getTasks(db.GetUsrConnected());

        return view;

    }

    @Override
    public void onResume()
    {
        super.onResume();

        ListAdapter listeAdapter = new ListeAdapter(getContext(), listeTache) ;
        lvListe.setAdapter(listeAdapter) ;
    }

    private void startViewActivity(int position)
    {

        Tache uneTache = listeTache.get(position);
        // Nous paramétrons les coordonnées sur laquelle nous souhaitons nous rendre
        Intent intent = new Intent(getContext(), DetailsTask.class);
        intent.putExtra("task_id", String.valueOf(uneTache.task_id));

        startActivity(intent);
    }

}





