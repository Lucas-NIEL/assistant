package com.btsinfo.assistant;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class Tab3Rappels extends Fragment {
    Button BT_Valider;
    DatabaseManager DB;
    String usr_id;
    EditText et_bloc_note;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {

        View view = inflater.inflate(R.layout.tab3rappels,container,false);

        et_bloc_note = (EditText) view.findViewById(R.id.et_bloc_note) ;

        // Récupération de l'usr connecté
        DB = new DatabaseManager(getContext());
        usr_id = DB.GetUsrConnected();
        //Toast.makeText(getContext(),"ID : "+usr_id,Toast.LENGTH_SHORT).show();

        et_bloc_note = (EditText) view.findViewById(R.id.et_bloc_note);
        et_bloc_note.setText(DB.getNotes(usr_id));

        // Bouton ajouter nouvelle catégorie
        BT_Valider = (Button) view.findViewById(R.id.BT_Valider);
        BT_Valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            if(DB.newNote(usr_id, et_bloc_note.getText().toString()))
            {
                Toast.makeText(getContext(),"Votre note a bien été modifiée" , Toast.LENGTH_LONG).show();
            }

            }
        });

        return view;
    }



}
