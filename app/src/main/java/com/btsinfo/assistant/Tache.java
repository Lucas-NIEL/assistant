package com.btsinfo.assistant;

public class Tache {
    long task_id;
    String task_title;
    String task_desc;
    long task_usr_id;
    long task_cat_id;
    String task_date;

    public Tache(long task_id, String task_title, String task_desc, long task_usr_id, long task_cat_id) {
        this.task_id = task_id;
        this.task_title = task_title;
        this.task_desc = task_desc;
        this.task_usr_id = task_usr_id;
        this.task_cat_id = task_cat_id;
    }

    public Tache() {

    }

    public long getTask_id() { return task_id; }
    public void setTask_id(long task_id) { this.task_id = task_id; }

    public String getTask_title() { return task_title; }
    public void setTask_title(String task_title) { this.task_title = task_title; }

    public String getTask_desc() { return task_desc; }
    public void setTask_desc(String task_desc) { this.task_desc = task_desc; }

    public long getTask_usr_id() { return task_usr_id; }
    public void setTask_usr_id(long task_usr_id) { this.task_usr_id = task_usr_id; }

    public long getTask_cat_id() { return task_cat_id; }
    public void setTask_cat_id(long task_cat_id) { this.task_cat_id = task_cat_id; }

    public String getTask_date() { return task_date; }
    public void setTask_date(String task_date) { this.task_date = task_date; }




}
