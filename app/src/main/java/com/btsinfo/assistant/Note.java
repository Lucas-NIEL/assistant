package com.btsinfo.assistant;

public class Note {
    long note_id;
    String note_text;
    long note_usr_id;

    public long getNote_id() {
        return note_id;
    }

    public void setNote_id(long note_id) {
        this.note_id = note_id;
    }

    public String getNote_text() {
        return note_text;
    }

    public void setNote_text(String note_text) {
        this.note_text = note_text;
    }

    public long getNote_usr_id() {
        return note_usr_id;
    }

    public void setNote_usr_id(long note_usr_id) {
        this.note_usr_id = note_usr_id;
    }
}
